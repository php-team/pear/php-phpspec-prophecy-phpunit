php-phpspec-prophecy-phpunit (2.3.0-1) unstable; urgency=medium

  [ Christophe Coevoet ]
  * Add more precise types for the prophesize method
  * Update changelog

  [ David Prévot ]
  * Revert "Force system dependencies loading"
  * Drop now useless symlink at build time

 -- David Prévot <taffit@debian.org>  Mon, 25 Nov 2024 12:55:08 +0100

php-phpspec-prophecy-phpunit (2.2.0-1) unstable; urgency=medium

  [ Christophe Coevoet ]
  * Update the changelog for 2.2.0

  [ Alessandro Lai ]
  * Allow PHPUnit 11

  [ David Prévot ]
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Sun, 03 Mar 2024 09:20:09 +0100

php-phpspec-prophecy-phpunit (2.1.0-1) unstable; urgency=medium

  [ Alessandro Lai ]
  * Add support for Phpunit 10 (Closes: #1039819)

  [ Christophe Coevoet ]
  * Update changelog for 2.1.0

 -- David Prévot <taffit@debian.org>  Mon, 11 Dec 2023 08:09:13 +0100

php-phpspec-prophecy-phpunit (2.0.2-2) unstable; urgency=medium

  * Don’t check local src/ path on tests

 -- David Prévot <taffit@debian.org>  Sun, 23 Apr 2023 09:00:28 +0200

php-phpspec-prophecy-phpunit (2.0.2-1) unstable; urgency=medium

  [ Alessandro Lai ]
  * Add test to reach 100% coverage

  [ func0der ]
  * Add @not-deprecated annotation to avoid PHPStan errors

  [ Christophe Coevoet ]
  * Update the changelog

  [ David Prévot ]
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Sat, 22 Apr 2023 16:37:00 +0200

php-phpspec-prophecy-phpunit (2.0.1-4) unstable; urgency=medium

  * Build-depend on php-phpspec-prophecy (Closes: #1019986)

 -- David Prévot <taffit@debian.org>  Sun, 18 Sep 2022 09:55:31 +0200

php-phpspec-prophecy-phpunit (2.0.1-3) unstable; urgency=medium

  * Simplify gbp import-orig
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Install and use phpabtpl(1) autoloaders
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Mon, 04 Jul 2022 13:58:45 +0200

php-phpspec-prophecy-phpunit (2.0.1-2) unstable; urgency=medium

  * Source upload now that everything is in place

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 17:22:19 -0400

php-phpspec-prophecy-phpunit (2.0.1-2~stage1) unstable; urgency=medium

  * Upload to unstable since PHPUnit 9 should be in Bullseye
  * Rename main branch to debian/latest (DEP-14)
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Mon, 14 Dec 2020 18:47:06 -0400

php-phpspec-prophecy-phpunit (2.0.1-1) experimental; urgency=medium

  [ Christophe Coevoet ]
  * Update the package description
  * Add support for PHP 8 (#25)
  * Update changelog for 2.0.1

  [ David Prévot ]
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Sun, 12 Jul 2020 11:48:12 -0400

php-phpspec-prophecy-phpunit (2.0.0-1) experimental; urgency=medium

  * Initial release (new phpunit build-dependency)

 -- David Prévot <taffit@debian.org>  Sun, 31 May 2020 08:29:06 -1000
